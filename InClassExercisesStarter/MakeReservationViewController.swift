//
//  MakeReservationViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore


class MakeReservationViewController: UIViewController {
    var name = ""
    var rest: [Int: String] = [:]

    @IBOutlet weak var sucessTextLabel: UILabel!
    // MARK: Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var seatsTextField: UITextField!
    
    // Mark: Firestore variables
    var db:Firestore!
    
    
    // MARK: Default Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        db = Firestore.firestore()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Actions
    @IBAction func buttonPressed(_ sender: Any) {
        print("pressed the button")
        let ref = db.collection("allRestaurants").whereField("Name", isEqualTo: nameTextField.text!)
      
        
        ref.getDocuments() {
            (querySnapshot, err) in
            if (err == nil){
                let citiesRef = self.db.collection("reservation")
                citiesRef.document().setData([
                    "name": self.name,
                    "restaurant": self.nameTextField.text!,
                    "day": self.dayTextField.text!,
                    "numSeats": self.seatsTextField.text!,
                    ])
            }
            else if let err = err {
                print("this restaurant is not in database")
                self.sucessTextLabel.text! = "Error getting documents: \(err)"
            }
        }
                let songsRef = self.db.collection("reservation").whereField("name", isEqualTo: self.name)
        
        print("Querying database")
        
        songsRef.getDocuments() {
            (querySnapshot, err) in
            
            // MARK: FB - Boilerplate code to get data from Firestore
            if let err = err {
                print("Error getting documents: \(err)")
                self.sucessTextLabel.text! = "Error getting documents: \(err)"
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    self.sucessTextLabel.text! = "\(document.documentID) => \(document.data())"
                }
            }
        }
    }
    
    
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let n3 = segue.destination as! SeeReservationsViewController
        n3.key = self.rest
    }


}
