//
//  RestaurantMapViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
import CoreLocation
import FirebaseFirestore

class RestaurantMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
  
    var db:Firestore!

    var name = ""
    var keyword: [Int: String] = [:]
    var manager:CLLocationManager!
    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("loaded the map screen")
        db = Firestore.firestore()
        manager = CLLocationManager()
        manager.delegate = self
        
        // how accurate do you want the lkocation to be?
        manager.desiredAccuracy = kCLLocationAccuracyBest
        
        // ask for permission to get the location
        manager.requestAlwaysAuthorization()
        
        // tell the manager to get the person's location
        manager.startUpdatingLocation()
        
        let x = CLLocationCoordinate2DMake(43.6532, -79.3832)
        let y = MKCoordinateSpanMake(0.01, 0.01)
        let z = MKCoordinateRegionMake(x, y)
        self.mapView.setRegion(z, animated: true)
        
       
        let URL = "https://opentable.herokuapp.com/api/restaurants?city=Toronto&per_page=5"
        
        // ALAMOFIRE function: get the data from the website
        Alamofire.request(URL, method: .get, parameters: nil).responseJSON {
            (response) in
            
            // -- put your code below this line
            
            if (response.result.isSuccess) {
                print("awesome, i got a response from the website!")
                print("Response from webiste: " )
                print(response.data)
                
                do {
                    let json = try JSON(data:response.data!)
                      let citiesRef = self.db.collection("allRestaurants")
                    
                    //print(json)
                    
                    // PARSING: grab the latitude and longitude
                    print(json["restaurants"][0]["name"])
                    print(json["restaurants"][0]["city"])
                    
                    print("------")
                    print("------")
              
                    let restaurants = json["restaurants"].array!
                    let restName = ""
                    var j = 0
                    
    
                    for x in restaurants {
                        
                        print(x["name"])
                        self.keyword[j] = x["name"].string!
                        
        
    
//                    print("-------")
//                    print("-------")
//
                   
                    let searchRequest = MKLocalSearchRequest()
                    searchRequest.naturalLanguageQuery = self.keyword[j]
                    
                    let coordinate = CLLocationCoordinate2DMake(43.6532, -79.3832)
                    let span = MKCoordinateSpanMake(0.05, 0.05)
                    let r = MKCoordinateRegionMake(coordinate, span)
                    
                    searchRequest.region = r
                    
                    let search = MKLocalSearch(request: searchRequest)
                    
                    search.start { (response, error) in
                        // put code here
                        // what do you want to do with the search results?
                        // Examples - show it in the terminal! show it in the ui! your choice!
                        //print(response?.mapItems)
                        
                        var places = response?.mapItems // [MKMapItems]
                        // do something with the results
                        for x in places! {
                            print("Name: \(x.name)")
                            print("Address: \(x.phoneNumber)")
                            print("Lat:  \(x.placemark.coordinate.latitude)")
                            print("Lng: \(x.placemark.coordinate.longitude)")
                            print("=======")
                          
                            
                            citiesRef.document().setData([
                                "Name" : "\(x.name!)",
                                "Address": "\(x.phoneNumber!)"
                                ])

                        let pin = MKPointAnnotation()
                        
                        // 2. Set the latitude / longitude of the pin
                            var y = CLLocationCoordinate2D(latitude: x.placemark.coordinate.latitude,longitude: x.placemark.coordinate.longitude)
                        pin.coordinate = y
                        
                        // 3. OPTIONAL: add a information popup (a "bubble")
                        pin.title = self.keyword[j]
                        
                        // 4. Show the pin on the map
                        self.mapView.addAnnotation(pin)
                        }
                        j = j+1
                        }
                        
                        
                        
                    }
                    
                }
                catch {
                    print ("Error while parsing JSON response")
                }
                
            }
            
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // MARK: Actions

    @IBAction func zoomInPressed(_ sender: Any) {
        
        print("zoom in!")
        
        print("plus button pressed!")
        var r = mapView.region
        
        print("Current zoom: \(r.span.latitudeDelta)")
        
        r.span.latitudeDelta = r.span.latitudeDelta / 4
        r.span.longitudeDelta = r.span.longitudeDelta / 4
        print("New zoom: \(r.span.latitudeDelta)")
        print("-=------")
        self.mapView.setRegion(r, animated: true)
        
        // HINT: Check MapExamples/ViewController.swift
    }
    
    @IBAction func zoomOutPressed(_ sender: Any) {
        // zoom out
        print("zoom out!")
        
        // HINT: Check MapExamples/ViewController.swift
        var r = mapView.region
        r.span.latitudeDelta = r.span.latitudeDelta * 2
        r.span.longitudeDelta = r.span.longitudeDelta * 2
        self.mapView.setRegion(r, animated: true)
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let n3 = segue.destination as! MakeReservationViewController
        n3.rest = self.keyword
    }

    

}
