//
//  MenuTableViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    var name = ""

     var i = 0
    var items = ["Restaurant Map", "Make a Reservation", "Show Reservation"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        i = indexPath.row
        
        print("Person clicked in row number: \(i)")
        
        
        if (i == 0) {
            performSegue(withIdentifier: "segueRestaurantMap", sender: self)
        }
        else if (i  == 1) {
            performSegue(withIdentifier: "segueMakeReservation", sender: self)
        }
        else if (i == 2) {
            performSegue(withIdentifier: "segueSeeReservation", sender: self)
        }
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let i = self.tableView.indexPathForSelectedRow?.row
        
        
        print("Selected row: \(i)")
        if ( i == 0){
            print("Restaurant Map Row Clicked")
            let n1 = segue.destination as! RestaurantMapViewController
            n1.name = name
        }
        if ( i  == 1){
            print("Make a Reservation Row Clicked")
            let n2 = segue.destination as! MakeReservationViewController
            n2.name = name
        }
        if ( i  == 2){
            print("Show Reservation Row Clicked")
            let n3 = segue.destination as! SeeReservationsViewController
            n3.name = name
        }
    }
   

}
